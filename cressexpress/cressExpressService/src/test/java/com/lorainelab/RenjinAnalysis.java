/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab;

import java.io.IOException;
import javax.script.ScriptException;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author dcnorris
 */
public class RenjinAnalysis {

//    ScriptEngine engine;
//    Map<String, Map<String, Double>> expressionData;
//    List<String> microarrayOrder;
//    List<String> probesetNames;
//    List<String> microarraySelections;
//    List<String> probesetSelections;
//    private final double plcRsquaredThreshold = 0.36;
//    private final String expressionFlatFile = "/home/dcnorris/NetBeansProjects/rcressexpress/AllAgainstAll/cressExpressFlatFileDump/exprRowData.txt";

    @Before
    public void setup() throws IOException {
//        ScriptEngineManager manager = new ScriptEngineManager();
//        engine = manager.getEngineByName("Renjin");
//        String microarrayOrderSource = CharStreams.toString(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("microarrayOrder.txt"), Charsets.UTF_8));
//        String probesetNamesSource = CharStreams.toString(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("probesetNames.txt"), Charsets.UTF_8));
//        String microarraySelectionsSource = CharStreams.toString(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("microarraySelections.txt"), Charsets.UTF_8));
//        String probesetSelectionsSource = CharStreams.toString(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("probesetSelections.txt"), Charsets.UTF_8));
//        microarrayOrder = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(microarrayOrderSource);
//        probesetNames = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(probesetNamesSource);
//        probesetSelections = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(probesetSelectionsSource);
//        microarraySelections = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(microarraySelectionsSource);
//
//        File dbFile = new File("/home/dcnorris/expressionData");
//        DB db = DBMaker.newFileDB(dbFile).mmapFileEnable().transactionDisable().asyncWriteEnable().cacheHardRefEnable().make();
//        expressionData = db.getTreeMap("map");
//        if (expressionData.isEmpty()) {
//            Reader in = new FileReader(expressionFlatFile);
//            for (CSVRecord record : CSVFormat.DEFAULT.parse(in)) {
//                String probesetName = record.get(0);
//                LinkedHashMap<String, Double> probesetExprValues = new LinkedHashMap<>(22_810);
//                for (int i = 1; i < record.size(); i++) {
//                    probesetExprValues.put(microarrayOrder.get(i - 1), Double.parseDouble(record.get(i)));
//                }
//                expressionData.put(probesetName, probesetExprValues);
//                db.commit();
//            }
//            db.compact();
//            db.commit();
//
//        }
    }

    @Test
    public void correlationAnalysis() throws ScriptException, IOException {
//        BiFunction<List<Double>, List<Double>, Double> cor = (x, y) -> {
//            double t = 0;
//            try {
//                engine.put("x", Doubles.toArray(x));
//                engine.put("y", Doubles.toArray(y));
//                t = ((SEXP) engine.eval("cor(x,y)")).asReal();
//            } catch (ScriptException ex) {
//                Logger.getLogger(RenjinAnalysis.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            return t;
//        };
//
//        Function<String, List<Double>> probesetExprVals = probeset -> microarraySelections.parallelStream().map(microarray -> expressionData.get(probeset).get(microarray)).collect(toList());
//        List<List<Double>> xVals = probesetSelections.parallelStream().map(probeset -> probesetExprVals.apply(probeset)).collect(toList());
//        List<List<Double>> yVals = probesetNames.parallelStream().map(probeset -> probesetExprVals.apply(probeset)).collect(toList());
//        List<Double> results = xVals.stream().map(x -> yVals.stream().map(y -> cor.apply(x, y))).flatMap(s -> s.parallel()).collect(toList());
//        results.stream().forEach(d -> System.out.println(d));
    }

}
