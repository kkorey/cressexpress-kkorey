package com.lorainelab.cressexpress.controller;

import com.lorainelab.cressexpress.facade.ProbesetFacade;
import com.lorainelab.cressexpress.model.Probeset;
import java.io.Serializable;
import java.util.List;
import java.util.TreeMap;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Stateless
@LocalBean
public class ProbesetController implements Serializable {

    @EJB
    private com.lorainelab.cressexpress.facade.ProbesetFacade ejbFacade;

    public ProbesetController() {
    }

    public ProbesetFacade getEjbFacade() {
        return ejbFacade;
    }   

    public TreeMap<String, Integer> getProbesetIdMap() {
        TreeMap<String, Integer> probesetIdMap = new TreeMap<>();
        List<Probeset> entities = findProbesetEntities();
        for (Probeset p : entities) {
            probesetIdMap.put(p.getName(), p.getId());
        }
        return probesetIdMap;
    }

    public List<Probeset> findProbesetEntities() {
        return findProbesetEntities(true, -1, -1);
    }

    public List<Probeset> findProbesetEntities(int maxResults, int firstResult) {
        return findProbesetEntities(false, maxResults, firstResult);
    }

    private List<Probeset> findProbesetEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = ejbFacade.getEntityManager();
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Probeset.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();

    }

    public Probeset findProbeset(Integer id) {
        EntityManager em = ejbFacade.getEntityManager();
        return em.find(Probeset.class, id);
    }

    public int getProbesetCount() {
        EntityManager em = ejbFacade.getEntityManager();
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Probeset> rt = cq.from(Probeset.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
