/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lorainelab.cressexpress.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dcnorris
 */
@Entity
@Table(name = "platform")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Platform.findAll", query = "SELECT p FROM Platform p"),
    @NamedQuery(name = "Platform.findById", query = "SELECT p FROM Platform p WHERE p.id = :id"),
    @NamedQuery(name = "Platform.findByName", query = "SELECT p FROM Platform p WHERE p.name = :name"),
    @NamedQuery(name = "Platform.findByMaker", query = "SELECT p FROM Platform p WHERE p.maker = :maker")})
public class Platform implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "maker")
    private String maker;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "platform")
    private Set<ReleaseVersion> releaseVersionSet;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "platformId")
    private Set<Probeset> probesetSet;

    public Platform() {
    }

    public Platform(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    @XmlTransient
    public Set<ReleaseVersion> getReleaseVersionSet() {
        return releaseVersionSet;
    }

    public void setReleaseVersionSet(Set<ReleaseVersion> releaseVersionSet) {
        this.releaseVersionSet = releaseVersionSet;
    }

    @XmlTransient
    public Set<Probeset> getProbesetSet() {
        return probesetSet;
    }

    public void setProbesetSet(Set<Probeset> probesetSet) {
        this.probesetSet = probesetSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Platform)) {
            return false;
        }
        Platform other = (Platform) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lorainelab.cressdbbuilder.Platform[ id=" + id + " ]";
    }
    
}
