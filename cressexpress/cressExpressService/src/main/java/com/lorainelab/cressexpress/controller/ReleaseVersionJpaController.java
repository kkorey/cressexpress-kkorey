/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lorainelab.cressexpress.controller;

import com.lorainelab.cressexpress.controller.exceptions.IllegalOrphanException;
import com.lorainelab.cressexpress.controller.exceptions.NonexistentEntityException;
import com.lorainelab.cressexpress.controller.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.lorainelab.cressexpress.model.Platform;
import com.lorainelab.cressexpress.model.ExprColumn;
import java.util.HashSet;
import java.util.Set;
import com.lorainelab.cressexpress.model.Job;
import com.lorainelab.cressexpress.model.ExprRow;
import com.lorainelab.cressexpress.model.ReleaseVersion;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author dcnorris
 */
public class ReleaseVersionJpaController implements Serializable {

    public ReleaseVersionJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    
    
}
