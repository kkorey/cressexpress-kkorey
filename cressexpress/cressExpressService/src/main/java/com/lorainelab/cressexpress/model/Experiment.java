/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;

/**
 *
 * @author dcnorris
 */
@Entity
@Indexed
@Table(name = "experiment")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Experiment.findAll", query = "SELECT e FROM Experiment e"),
    @NamedQuery(name = "Experiment.findById", query = "SELECT e FROM Experiment e WHERE e.id = :id"),
    @NamedQuery(name = "Experiment.findByName", query = "SELECT e FROM Experiment e WHERE e.name = :name"),
    @NamedQuery(name = "Experiment.findByTitle", query = "SELECT e FROM Experiment e WHERE e.title = :title"),
    @NamedQuery(name = "Experiment.findBySummary", query = "SELECT e FROM Experiment e WHERE e.summary = :summary")})
public class Experiment implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Field
    @Column(name = "name")
    private String name;
    @Field
    @Column(name = "title")
    private String title;
    @Field
    @Column(name = "summary")
    private String summary;
    
    @IndexedEmbedded
    @ManyToMany(mappedBy = "experimentSet", fetch = FetchType.EAGER)
    private Set<Microarray> microarraySet;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "experiment")
    private Set<ExperimentAttribute> experimentAttributeSet;

    public Experiment() {
    }

    public Experiment(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    @XmlTransient
    public Set<Microarray> getMicroarraySet() {
        return microarraySet;
    }

    public void setMicroarraySet(Set<Microarray> microarraySet) {
        this.microarraySet = microarraySet;
    }

    public Set<ExperimentAttribute> getExperimentAttributeSet() {
        return experimentAttributeSet;
    }

    public void setExperimentAttributeSet(Set<ExperimentAttribute> experimentAttributeSet) {
        this.experimentAttributeSet = experimentAttributeSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Experiment)) {
            return false;
        }
        Experiment other = (Experiment) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lorainelab.cressdbbuilder.Experiment[ id=" + id + " ]";
    }

}
