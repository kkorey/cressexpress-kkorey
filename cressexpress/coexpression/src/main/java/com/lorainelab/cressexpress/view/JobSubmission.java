/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.view;

import com.google.common.base.Joiner;
import com.lorainelab.cressexpress.controller.JobController;
import com.lorainelab.cressexpress.model.Job;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import javax.annotation.Resource;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jms.JMSContext;
import javax.jms.Queue;
import org.apache.commons.validator.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dcnorris
 */
@Named
@Stateful
@SessionScoped
public class JobSubmission implements Serializable{
    
    private static final long serialVersionUID = 6526496455322776257L;

    private static final Logger logger = LoggerFactory.getLogger(JobSubmission.class);

    @Inject
    private GeneSelection geneSelection;
    @Inject
    private MicroarraySelections microarraySelections;
    @Inject
    private ReleaseVersionSelection releaseVersionSelection;
    @Inject
    private ThresholdSelection thresholdSelection;

    @Inject
    private transient JMSContext jmsContext;

    @Resource(lookup = "java:/queue/JobProccessor")
    private Queue jobProccessorQueue;

    @Inject
    private JobController jobController;

    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void validateEmailAddress(FacesContext fc, UIComponent validate, Object value) {
        String email = value.toString();
        boolean isValid = EmailValidator.getInstance().isValid(email);
        if (!isValid) {
            fc.validationFailed();
            FacesMessage msg = new FacesMessage("Invalid email format");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            fc.addMessage("Validation failed:", msg);
            fc.renderResponse();
        }
    }

    public void navigateNext() throws IOException {
        handleJobCreation();        
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        externalContext.redirect(externalContext.getRequestContextPath() + "/submissionReview.xhtml");
    }

    private void handleJobCreation() {
        Job job = new Job();
        job.setDate(new Date());
        job.setEmail(email);
        job.setPlcRsquared((float) thresholdSelection.getThreshold());
        job.setReleaseVersion(releaseVersionSelection.getSelectedVersion());
        job.setArraySelections(Joiner.on(",").skipNulls().join(microarraySelections.getSelectedArrayIdentifiers()));
        job.setProbesetSelections(Joiner.on(",").skipNulls().join(geneSelection.getValidProbeSets()));
        jobController.create(job);
        sendJobToProcessorQueue(job);
    }

    private void sendJobToProcessorQueue(Job job) {
        jmsContext.createProducer().send(jobProccessorQueue, job);
    }

    public void navigatePrev() throws IOException {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        context.redirect(context.getRequestContextPath() + "/step4.xhtml");
    }
}
