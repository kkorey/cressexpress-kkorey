/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.webservice;

import com.google.common.base.Splitter;
import com.lorainelab.cressexpress.processor.DefaultJobProcessor;
import com.lorainelab.cressexpress.reference.AgiMappingReference;
import com.lorainelab.cressexpress.reference.ProbesetInfoReference;
import com.lorainelab.cressexpress.reference.ReleaseVersionReference;
import com.lorainelab.cressexpress.webservice.exceptions.InvalidInputException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.DependsOn;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.hornetq.utils.json.JSONArray;
import org.hornetq.utils.json.JSONException;
import org.hornetq.utils.json.JSONObject;

/**
 *
 * @author dcnorris
 */
@Path("/")
@Startup
@DependsOn({"DefaultJobProcessor", "ReleaseVersionReference"})
@Singleton
public class CressexpressWebService {

    @Inject
    private ProbesetInfoReference probesetInfoReference;

    @Inject
    private DefaultJobProcessor defaultJobProcessor;

    @Inject
    private ReleaseVersionReference releaseVersionReference;

    @Inject
    private AgiMappingReference agiMappingReference;

    private Set<String> validProbesetNames;
    private Set<Double> validReleaseVersions;

    @PostConstruct
    public void setup() {
        validReleaseVersions = new HashSet<>();
        validProbesetNames = probesetInfoReference.getProbesetNames();
        releaseVersionReference.getAllReleaseVersions().stream().forEach(releaseVersion -> validReleaseVersions.add(Double.parseDouble(releaseVersion.getName())));
    }

    @Path("/expressionData")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String retrieveExpressionData(@QueryParam("pss") String probesetInput, @QueryParam("release") double version, @QueryParam("format") String format) {
        Set<String> probesetList = new HashSet<>();
        probesetList.addAll(Splitter.on(",").omitEmptyStrings().trimResults().splitToList(probesetInput));
        if (probesetList.size() > 30) {
            throw new InvalidInputException("Our service currently only allows 30 probesets to be selected per run");
        }
        for (String probesetName : probesetList) {
            if (!validProbesetNames.contains(probesetName)) {
                throw new InvalidInputException(probesetName + " is not a valid probeset");
            }
        }
        if (!validReleaseVersions.contains(version)) {
            throw new InvalidInputException(version + " is not a valid release");
        }
        if (StringUtils.isNotBlank(format) && format.equalsIgnoreCase("json")) {
            return new ExpressionDataResponse(probesetList, version).toJson();
        }
        return new ExpressionDataResponse(probesetList, version).toString();
    }

    private class ExpressionDataResponse {

        String[] COLUMN_HEADERS = {"GSM ID", "GSM DESCRIPTION"};
        String TAB = "\t";
        String NEWLINE = "\n";
        StringBuilder tsvContent;
        List<String> probesetNames;

        public ExpressionDataResponse(Set<String> probesetNames, double version) {
            this.probesetNames = new ArrayList<>();
            this.probesetNames.addAll(probesetNames);
            tsvContent = new StringBuilder();
            createHeaderLine(probesetNames);
            //for now don't handle version differences
            defaultJobProcessor.appendExpressionData(probesetNames, tsvContent);
        }

        private void createHeaderLine(Set<String> probesetNames) {
            for (String columnHeader : COLUMN_HEADERS) {
                tsvContent.append(columnHeader);
                tsvContent.append(TAB);
            }
            probesetNames.stream().forEach(probesetName -> {
                tsvContent.append(probesetName);
                tsvContent.append(TAB);
            });
            tsvContent.append(NEWLINE);
        }

        @Override
        public String toString() {
            return tsvContent.toString();
        }

        public String toJson() {
            JSONObject toReturn = new JSONObject();
            JSONArray list = new JSONArray();
            try {
                int i = 0;
                for (CSVRecord record : CSVFormat.DEFAULT.parse(new StringReader(tsvContent.toString()))) {
                    JSONObject jsonString = new JSONObject();
                    //skip header
                    if (i > 0) {
                        List<String> lines = Splitter.on("\t").omitEmptyStrings().trimResults().splitToList(record.get(0));
                        if (!lines.isEmpty() && lines.size() == probesetNames.size() + 2) {
                            jsonString.put("GSM ID", lines.get(0));
                            jsonString.put("GSM DESCRIPTION", lines.get(1));
                            for (int j = 0; j < probesetNames.size(); j++) {
                                String probesetName = probesetNames.get(j);
                                jsonString.put(probesetName, lines.get(j + 2));
                            }

                            list.put(jsonString);
                        }
                    }

                    i++;
                }
                toReturn.put("ExpressionData", list);
            } catch (JSONException | IOException ex) {
                Logger.getLogger(CressexpressWebService.class.getName()).log(Level.SEVERE, null, ex);
            }

            return toReturn.toString();
        }

    }
}
