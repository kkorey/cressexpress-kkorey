/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lorainelab.cressexpress.view.converters;

import com.lorainelab.cressexpress.controller.ReleaseVersionController;
import com.lorainelab.cressexpress.model.ReleaseVersion;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author dcnorris
 */
@FacesConverter(value = "releaseVersionConverter")
public class ReleaseVersionConverter implements Converter  {
    
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        // Convert unique String representation of ProjectDetail back to ProjectDetail object.
        ReleaseVersion matchMethod = (ReleaseVersion) value;
        String idAsString = String.valueOf(matchMethod.getId());
        return idAsString;
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        // Convert ProjectDetail to its unique String representation.
        ReleaseVersionController releaseVersionController = null;
        try {
            InitialContext ic = new InitialContext();
            releaseVersionController = (ReleaseVersionController) ic.lookup("java:global/coexpression-1.0/ReleaseVersionController");
        } catch (NamingException e) {
            e.printStackTrace();
        }

        Integer id = Integer.parseInt(value);
        ReleaseVersion matchMethod = releaseVersionController.findReleaseVersion(id);
        return matchMethod;
    }

}
