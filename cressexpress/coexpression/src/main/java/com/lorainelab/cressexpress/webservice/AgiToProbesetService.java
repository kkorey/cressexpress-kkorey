/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.webservice;

import com.lorainelab.cressexpress.reference.AgiMappingReference;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.DependsOn;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.apache.commons.lang.StringUtils;
import org.hornetq.utils.json.JSONArray;
import org.hornetq.utils.json.JSONException;
import org.hornetq.utils.json.JSONObject;

/**
 *
 * @author dcnorris
 */
@Path("/")
@Startup
@DependsOn({"DefaultJobProcessor", "ReleaseVersionReference"})
@Singleton
public class AgiToProbesetService {

    @Inject
    private AgiMappingReference agiMappingReference;

    @Path("/agiToProbeset")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getProbesetListFromAGI(@QueryParam("agi") String agiID) {
        JSONObject toReturn = new JSONObject();
        JSONArray list = new JSONArray();
        try {
            if (StringUtils.isNotBlank(agiID)) {
                agiID = agiID.toUpperCase();
                if (agiMappingReference.getValidAGINames().contains(agiID)) {
                    Collection<String> probesetSetNames = agiMappingReference.getAgiToProbesetMapping().asMap().get(agiID);
                    for (String probeset : probesetSetNames) {
                        JSONObject jsonString = new JSONObject();
                        jsonString.put("probeset", probeset);
                        list.put(jsonString);
                    }

                } else {
                    JSONObject jsonString = new JSONObject();
                    jsonString.put("Error", "Invalid gene name");
                }
            } else {
                JSONObject jsonString = new JSONObject();
                jsonString.put("Error", "Invalid gene name");
            }
            toReturn.put("ProbesetList", list);
        } catch (JSONException ex) {
            Logger.getLogger(CressexpressWebService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return toReturn.toString();
    }

}
