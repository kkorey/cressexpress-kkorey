package CressExpress_data_generation;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author lorainelab
 */
import java.sql.DriverManager;
import java.sql.*;
import java.io.*;

public class Array_attribute {

    public static void main(String[] args) {


        try {

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String user = "ndahake";
            String pass = "itfungtopove1!";

            String URL = "jdbc:mysql://localhost:6963/coexpression2?autoReconnect=true";//forwarded port 6963 on local machine to db.transvar.org:3306 using putty
// static String URL = "jdbc:mysql://db.transvar.org:3306/coexpression2?autoReconnect=true"; // uncomment if running program on a computer on the UNC Charlotte network. 

            String DB_name = "coexpression2";

            
            
            Connection conn = DriverManager.getConnection(URL, user, pass);


            String inputFile = args[0];
            String outputFile = args[1];

            FileReader fr = new FileReader(inputFile);
            BufferedReader br = new BufferedReader(fr);

            FileWriter fw = new FileWriter(outputFile);
            BufferedWriter bw = new BufferedWriter(fw);

            String SQL = "SELECT id FROM array WHERE name = ";
            ResultSet res;
            Statement stmnt = conn.createStatement();
            String line;
            String finalLine = "";
            while ((line = br.readLine()) != null) {
                line = line.trim();
                String arr[] = line.split("\t");
                res = stmnt.executeQuery(SQL + "'" + arr[0] + "'");
                String id = " ";
                while (res.next()) {
                    id = res.getString("id");
                }
                arr[0] = id;
                finalLine = "";
                for (int i = 0; i < arr.length; i++) {
                    finalLine = finalLine + arr[i] + "\t";
                }

                finalLine = finalLine.trim();
                bw.write(finalLine + "\n");
            }
            bw.flush();
            fw.flush();
            bw.close();
            fw.close();

        } catch (Exception e) {
            System.out.println(e.toString());
        }

    }
}
