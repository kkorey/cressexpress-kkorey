#!/usr/bin/env python 

doc=\
"""
Uses Aspera Connect command line program ascp to retrieve GEO CEL files.
"""

# this was helpful:
# http://www.1000genomes.org/faq/how-download-files-using-aspera


import argparse,os,sys

cmd = '(ascp -QT -l640M -i %s anonftp@ftp-private.ncbi.nlm.nih.gov:%s %s && echo S) || echo F'
  
def main(fname=None,download_dir=None,
         key='~/Applications/Aspera Connect.app/Contents/Resources/asperaweb_id_dsa.putty'):
    lines=open(fname).readlines()
    for line in lines:
        link=line.strip()
        try:
            getData(link=line)
        except Exception:
            
def getData(link=None,report=sys.stderr):
    command=cmd%(key,link,download_dir)
    message=str(os.popen(commandToExec).read())
    if message.endswith('F\n'):
        report.write('%s %s'%(link,message)

            
if __name__ == '__main__':
      filename=sys.argv[1]
      download_dir = sys.argv[2]
      failed_links_filename = str(sys.argv[3])
      main(fname=filename,download_dir=download_dir,
              file_links_filanme=failed_links_filename)
