-- MySQL dump 10.13  Distrib 5.7.21, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: cressexpress
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `assembly`
--

DROP TABLE IF EXISTS `assembly`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assembly` (
  `id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `species` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_assembly_species1_idx` (`species`),
  CONSTRAINT `fk_species_id` FOREIGN KEY (`species`) REFERENCES `species` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `experiment`
--

DROP TABLE IF EXISTS `experiment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experiment` (
  `id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `title` varchar(1000) DEFAULT NULL,
  `summary` varchar(1800) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `experiment2array`
--

DROP TABLE IF EXISTS `experiment2array`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experiment2array` (
  `microarray` int(11) NOT NULL,
  `experiment` int(11) NOT NULL,
  KEY `fk_experiment2array_array1_idx` (`microarray`),
  KEY `fk_experiment2array_experiment1_idx` (`experiment`),
  CONSTRAINT `fk_array_id2` FOREIGN KEY (`microarray`) REFERENCES `microarray` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_experiment_id2` FOREIGN KEY (`experiment`) REFERENCES `experiment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `experimentattribute`
--

DROP TABLE IF EXISTS `experimentattribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experimentattribute` (
  `name` varchar(64) NOT NULL,
  `value` varchar(7000) NOT NULL,
  `namespace` varchar(64) DEFAULT NULL,
  `experiment` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `fk_experimentAttribute_1` (`experiment`),
  CONSTRAINT `fk_experimentAttribute_1` FOREIGN KEY (`experiment`) REFERENCES `experiment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1605 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `exprcolumn`
--

DROP TABLE IF EXISTS `exprcolumn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exprcolumn` (
  `releaseVersion` int(11) NOT NULL,
  `microarray` int(11) NOT NULL,
  `data` mediumtext NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `fk_release_id` (`releaseVersion`),
  KEY `fk_array_id` (`microarray`),
  CONSTRAINT `fk_exprColumn_1` FOREIGN KEY (`releaseVersion`) REFERENCES `releaseversion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_exprColumn_2` FOREIGN KEY (`microarray`) REFERENCES `microarray` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8942 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `exprrow`
--

DROP TABLE IF EXISTS `exprrow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exprrow` (
  `data` mediumtext NOT NULL,
  `releaseVersion` int(11) NOT NULL,
  `probeset` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `fk_expr_row_release1_idx` (`releaseVersion`),
  KEY `fk_expr_row_probeset1_idx` (`probeset`),
  CONSTRAINT `fk_exprRow_1` FOREIGN KEY (`releaseVersion`) REFERENCES `releaseversion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_exprRow_2` FOREIGN KEY (`probeset`) REFERENCES `probeset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22811 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gene`
--

DROP TABLE IF EXISTS `gene`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gene` (
  `id` int(11) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `species` int(11) NOT NULL,
  `symbol` varchar(150) DEFAULT NULL,
  `description` varchar(1800) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gene_species1_idx` (`species`),
  CONSTRAINT `fk_gene_species1` FOREIGN KEY (`species`) REFERENCES `species` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `job`
--

DROP TABLE IF EXISTS `job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job` (
  `email` varchar(50) NOT NULL,
  `probesetSelections` mediumtext NOT NULL,
  `arraySelections` mediumtext NOT NULL,
  `plcRsquared` float NOT NULL,
  `date` date NOT NULL,
  `releaseVersion` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `fk_job_release1_idx` (`releaseVersion`),
  CONSTRAINT `fk_job_release1` FOREIGN KEY (`releaseVersion`) REFERENCES `releaseversion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `matchmethod`
--

DROP TABLE IF EXISTS `matchmethod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matchmethod` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `notes` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `microarray`
--

DROP TABLE IF EXISTS `microarray`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `microarray` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `summary` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `microarrayattribute`
--

DROP TABLE IF EXISTS `microarrayattribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `microarrayattribute` (
  `namespace` varchar(64) DEFAULT NULL,
  `name` varchar(64) NOT NULL,
  `value` varchar(512) NOT NULL,
  `microarray` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `fk_array_id` (`microarray`),
  CONSTRAINT `fk_array_id` FOREIGN KEY (`microarray`) REFERENCES `microarray` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=31021 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `platform`
--

DROP TABLE IF EXISTS `platform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `platform` (
  `id` int(11) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `maker` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `probeset`
--

DROP TABLE IF EXISTS `probeset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `probeset` (
  `id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `platform_id` int(11) NOT NULL,
  `uniq` varchar(45) DEFAULT NULL,
  `Redundant` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_probeset_platform1_idx` (`platform_id`),
  CONSTRAINT `fk_probeset_platform1` FOREIGN KEY (`platform_id`) REFERENCES `platform` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `probeset2genemap`
--

DROP TABLE IF EXISTS `probeset2genemap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `probeset2genemap` (
  `matchMethod` int(11) NOT NULL,
  `probeset` int(11) NOT NULL,
  `gene` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `fk_probeset2gene_match_method1_idx` (`matchMethod`),
  KEY `fk_probeset2gene_probeset1_idx` (`probeset`),
  KEY `fk_probeset2gene_gene1_idx` (`gene`),
  CONSTRAINT `fk_gene_id` FOREIGN KEY (`gene`) REFERENCES `gene` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_matchmethod_id` FOREIGN KEY (`matchMethod`) REFERENCES `matchmethod` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_probeset_id` FOREIGN KEY (`probeset`) REFERENCES `probeset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23585 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `releasedetails`
--

DROP TABLE IF EXISTS `releasedetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `releasedetails` (
  `celfile` varchar(60) NOT NULL,
  `releaseVersion` int(11) NOT NULL,
  `microarray` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `fk_gsm2gsm_file_release1_idx` (`releaseVersion`),
  KEY `fk_gsm2gsm_file_array1_idx` (`microarray`)
) ENGINE=InnoDB AUTO_INCREMENT=8942 DEFAULT CHARSET=euckr;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `releaseversion`
--

DROP TABLE IF EXISTS `releaseversion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `releaseversion` (
  `id` int(11) NOT NULL,
  `name` varchar(16) NOT NULL,
  `descr` varchar(256) DEFAULT NULL,
  `arrayOrder` mediumtext NOT NULL,
  `probesetOrder` mediumtext NOT NULL,
  `platform` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_release_platform1_idx` (`platform`),
  CONSTRAINT `fk_release_platform1` FOREIGN KEY (`platform`) REFERENCES `platform` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `species`
--

DROP TABLE IF EXISTS `species`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `species` (
  `id` int(11) NOT NULL,
  `genus` varchar(64) NOT NULL,
  `species` varchar(64) NOT NULL,
  `variety` varchar(64) DEFAULT NULL,
  `taxId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-23 14:22:50
